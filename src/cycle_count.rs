use crate::array_tools::{
    absolute_value_maximum, diff, find_unique_neighbors_reduced, find_unique_neighbors_with_diff,
    is_peak, is_unique,
};
use ndarray::{arr1, s, Array1, Array2, ArrayD, ArrayView1, Zip};
use pyo3::{exceptions::PyValueError, ffi::setattrofunc, PyResult};

/// Finds alternating peaks in a signal based on relative tolerance provided
///
/// This method is based on the pyyeti find_ap() method linked here  
/// [pyyeti find_ap()](https://github.com/twmacro/pyyeti/blob/master/pyyeti/cyclecount.py#L163C4-L163C4)
///
/// # Arguments
/// * `arr` times series array of values
/// * `tol` relative tolerance (default = 1e-6)
pub fn find_alternating_peaks(arr: &ArrayView1<f64>, tol: &Option<f64>) -> (Array1<bool>, usize) {
    if arr.len() == 1 {
        return (Array1::from_elem(arr.len(), true), 1);
    }
    let (unique, diff) = find_unique_neighbors_with_diff(&arr, tol.clone());

    let mut unique_indexes = Vec::new();
    for idx in 0..(unique.len()) {
        if unique[idx] {
            unique_indexes.push(idx);
        }
    }

    let mut unique_array = Array1::from_elem(unique_indexes.len(), 0.0);
    let mut unique_diff = Array1::from_elem(unique_indexes.len() - 1, 0.0);
    let unique_diff_len = unique_diff.len();
    for (idx, value) in unique_indexes.iter().enumerate() {
        unique_array[idx] = arr[*value];
        if idx < unique_diff_len {
            unique_diff[idx] = diff[unique_indexes[idx + 1] - 1];
        }
    }
    // Note pulling out unique.len() into its own value is of no use as from_elem will not take borrowed value
    let mut alternating_peaks = Array1::from_elem(unique.len(), false);
    alternating_peaks[0] = true;
    let len_arr = unique_array.len();
    // precomputing peak_count to speed up rainflow calculations later
    let mut peak_count: usize = 1;
    // Equivalent to python [1:-1] for array
    // println!("{:?}", &unique_array);
    for idx in 1..(&len_arr - 1) {
        // println!(
        //     "{}, Diff Last {}, Diff Next {}",
        //     unique_array[idx],
        //     unique_diff[idx - 1],
        //     unique_diff[idx]
        // );
        if unique_diff[idx].is_sign_positive() != unique_diff[idx - 1].is_sign_positive() {
            let big_array_index = unique_indexes[idx];
            alternating_peaks[big_array_index] = true;
            peak_count += 1;
        }
    }
    // println!("{},{}", &arr, &alternating_peaks);
    if unique_array[len_arr - 1] != unique_array[len_arr - 2] {
        // println!(
        // "Final Stage: {},{}",
        // unique_array[len_arr - 1],
        // unique_array[len_arr - 2]
        // );
        let big_array_index = unique_indexes[len_arr - 1];
        alternating_peaks[big_array_index] = unique_array[len_arr - 1] != unique_array[len_arr - 2];
        peak_count += 1
    }
    // println!("{},{}", &arr, &alternating_peaks);
    (alternating_peaks, peak_count)
}

pub fn find_alternating_peaks_reduced(
    arr: &ArrayView1<f64>,
    tol: &Option<f64>,
) -> (Vec<usize>, usize) {
    if arr.len() == 1 {
        return (Vec::from([0usize]), 1);
    }
    let (unique_indexes, diff) = find_unique_neighbors_reduced(&arr, tol.clone());

    // Note pulling out unique.len() into its own value is of no use as from_elem will not take borrowed value
    let mut alternating_peaks = Vec::with_capacity(unique_indexes.len());
    alternating_peaks.push(0usize);

    let mut peak_count: usize = 1;
    let len_unique_idx = unique_indexes.len();
    // Equivalent to python [1:-1] for array
    // println!("{:?}", &unique_array);
    for idx in 1..(&len_unique_idx - 1) {
        // println!(
        //     "{}, Diff Last {}, Diff Next {}",
        //     unique_array[idx],
        //     unique_diff[idx - 1],
        //     unique_diff[idx]
        // );
        let diff_idx = idx;
        if diff[diff_idx].is_sign_positive() != diff[diff_idx - 1].is_sign_positive() {
            alternating_peaks.push(idx);
            peak_count += 1;
        }
    }
    // println!("{},{}", &arr, &alternating_peaks);
    let last_unique_idx = unique_indexes[&len_unique_idx - 1];
    let second_last_unique_idx = unique_indexes[&len_unique_idx - 2];
    if arr[last_unique_idx] != arr[second_last_unique_idx] {
        // println!(
        // "Final Stage: {},{}",
        // unique_array[len_arr - 1],
        // unique_array[len_arr - 2]
        // );
        alternating_peaks.push(last_unique_idx);
        peak_count += 1
    }
    // println!("{},{}", &arr, &alternating_peaks);
    (alternating_peaks, peak_count)
}

/// Reduces array only to alternating peak values that are unique
///
/// Uses preallocated array based on `peak_count` size  
///
/// NOTE: Worth benchmarking to see if this actually a benefit to reduce this array
///
/// # Arguments
/// * `arr` times series array of values
/// * `bool_arr` boolean array of alternating unique peaks from `find_alternating_peaks
/// * `peak_count` count of peak values for static array allocation
pub fn reduce_arr_to_peaks(
    arr: &ArrayView1<f64>,
    bool_arr: &Array1<bool>,
    peak_count: &usize,
) -> Array1<f64> {
    // choosing to preallocate the array to speed up computation
    let mut alternating_peaks = Array1::from_elem(*peak_count, 0_f64);
    let mut peak_index = 0;
    for idx in 0..arr.len() {
        if bool_arr[idx] == true {
            alternating_peaks[peak_index] = arr[idx];
            peak_index += 1;
            // println!("peak idx {}, peak len {}", peak_index, peak_count);
        }
    }
    alternating_peaks
}

/// Finds alternating peaks in a signal based on relative tolerance provided
///
/// This method is a sped up version of the pyyeti implementation that reduces loops through the array
///
/// # Arguments
/// * `arr` times series array of values
/// * `tol` relative tolerance (default = 1e-6)
pub fn fast_find_alternating_peaks(
    arr: &ArrayView1<f64>,
    tol: Option<f64>,
) -> (Array1<bool>, usize) {
    if arr.len() == 1 {
        return (Array1::from_elem(arr.len(), true), 1usize);
    }

    let tol = match tol {
        Some(x) => x,
        None => 1e-6,
    };

    let diff = diff(&arr);
    let max = absolute_value_maximum(&diff.slice(s![..]));
    let stol = (tol * max).abs();
    let len_arr = arr.len();
    let mut alternating_peaks = Array1::from_elem(len_arr, false);
    let mut last_diff = &diff[0];
    alternating_peaks[0] = true;
    let mut peak_count = 0;
    let mut last_unique: usize = 0;

    // precomputing peak_count to speed up rainflow calculations later
    // println!("Evaluating array {:?}", arr);
    for idx in 1..(&len_arr - 2) {
        // println!("Index {}", idx);
        if is_unique(&diff[idx - 1], &stol) {
            last_unique = idx.clone();
            // println!("Value is unique:{}", arr[idx]);
            if is_peak(&diff[idx - 1], &last_diff) {
                alternating_peaks[idx] = true;
                peak_count += 1;
                // println!("Value is peak:{}", arr[idx]);
                last_diff = &diff[idx - 1];
            }
        } else {
            if is_peak(&diff[idx], &last_diff) {
                alternating_peaks[last_unique] = true;
                peak_count += 1;
                last_diff = &diff[last_unique - 1];
                // println!("Value is peak:{}", arr[last_unique]);
            }
        }
    }

    if is_peak(&diff[&len_arr - 3], &last_diff)
        && is_unique(&diff[&len_arr - 3], &stol)
    {
        alternating_peaks[&len_arr - 2] = true;
        peak_count += 1;    
    }
    // Checking edge case for last index
    if is_unique(&diff[&len_arr - 2], &stol) {
        alternating_peaks[&len_arr - 1] = true;
        peak_count += 1;    

        // Don't think about this if statement too much.  Tom is a genius and you know it
        // TODO: needs review
        if !(is_peak(&diff[&len_arr - 2], &diff[&len_arr - 3]))
        {
            if alternating_peaks[&len_arr - 2] {
                peak_count -= 1;
                alternating_peaks[&len_arr - 2] = false;
            }
        }
    }

    // if arr[len_arr - 1] != arr[last_unique] {
    //     alternating_peaks[len_arr - 1] = arr[len_arr - 1] != arr[last_unique];
    //     peak_count += 1;
    // }
    (alternating_peaks, peak_count)
}

/// Rainflow counting of array values
///
/// # Arguments
/// * `arr` times series array of values
/// * `tol` relative tolerance for alternating peak finding (default = 1e-6)
pub fn rainflow(arr: &ArrayView1<f64>, tol: &Option<f64>) -> PyResult<Array2<f64>> {
    todo!();
    let len_arr = arr.len();
    if *&len_arr < 2 {
        let message = format!("Array length must >= 2. Array length is {}", &len_arr);
        return Err(PyValueError::new_err(message));
    }
    let (alternating_peaks_bools, peak_count) = find_alternating_peaks(&arr, &tol);
    let alternating_peaks = reduce_arr_to_peaks(arr, &alternating_peaks_bools, &peak_count);

    Ok(Array2::from_elem((len_arr, 3_usize), 100.0))
}

mod test {
    use super::*;
    #[test]
    fn test_fast_find_alternating_peaks() {
        let arr = Array1::from_vec(vec![1.0, 2.0, 4.0, 4.0, 5.0]);
        let (booleans, _) = fast_find_alternating_peaks(&arr.view(), None);
        assert_eq!(Array1::from_vec(vec![true, false, true, false]), booleans)
    }
}
