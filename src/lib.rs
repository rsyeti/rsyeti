use ndarray::Array1;
use numpy::{PyArray1, PyArray2, PyReadonlyArray1, ToPyArray};
use pyo3::prelude::*;

pub mod array_tools;
pub mod cycle_count;

/// Finds elementwise diff of array
#[pyfunction]
#[pyo3(name = "diff")]
fn diff_py<'py>(py: Python<'py>, diff_arr: PyReadonlyArray1<f64>) -> &'py PyArray1<f64> {
    // borrowing with try_readonly is about 30% faster due to lack of clone
    let binding = diff_arr.try_readonly().unwrap();
    let diff_arr = binding.as_array();
    (array_tools::diff(&diff_arr)).to_pyarray(py)
}

/// Finds unique values of a numpy float64 array
///
/// Returns:
///   True/False array dictating whether element is unique or not
#[pyfunction]
#[pyo3(name = "find_unique_neighbors")]
fn find_unique_py<'py>(
    py: Python<'py>,
    arr: PyReadonlyArray1<f64>,
    tol: Option<f64>,
) -> PyResult<&'py PyArray1<bool>> {
    // borrowing with try_readonly is about 30% faster due to lack of clone
    let binding = arr.try_readonly().unwrap();
    let arr = binding.as_array();
    Ok(array_tools::find_unique_neighbors(arr, tol).to_pyarray(py))
}

#[pyfunction]
#[pyo3(name = "find_alternating_peaks")]
fn find_alternating_peaks_py<'py>(
    py: Python<'py>,
    arr: PyReadonlyArray1<f64>,
    tol: Option<f64>,
) -> &'py PyArray1<bool> {
    let binding = arr.try_readonly().unwrap();
    let arr = binding.as_array();
    cycle_count::find_alternating_peaks(&arr, &tol)
        .0
        .to_pyarray(py)
}

#[pyfunction]
#[pyo3(name = "fast_find_alternating_peaks")]
fn fast_find_alternating_peaks_py<'py>(
    py: Python<'py>,
    arr: PyReadonlyArray1<f64>,
    tol: Option<f64>,
) -> &'py PyArray1<bool> {
    let binding = arr.try_readonly().unwrap();
    let arr = binding.as_array();
    cycle_count::fast_find_alternating_peaks(&arr, tol)
        .0
        .to_pyarray(py)
}

#[pyfunction]
#[pyo3(name = "rainflow")]
fn rainflow_py<'py>(
    py: Python<'py>,
    arr: PyReadonlyArray1<f64>,
    tol: Option<f64>,
) -> PyResult<&'py PyArray2<f64>> {
    let binding = arr.try_readonly().unwrap();
    let arr = binding.as_array();

    // unpacking PyResult<ArrayBase, PyErr> to the proper response for python
    match cycle_count::rainflow(&arr, &tol) {
        Ok(val) => Ok(val.to_pyarray(py)),
        Err(err) => Err(err),
    }
}

/// A Python module implemented in Rust.
/// NOTE: this fn must be named the module name
#[pymodule]
fn chuchuna(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(diff_py, m)?)?;
    m.add_function(wrap_pyfunction!(find_unique_py, m)?)?;
    m.add_function(wrap_pyfunction!(find_alternating_peaks_py, m)?)?;
    m.add_function(wrap_pyfunction!(fast_find_alternating_peaks_py, m)?)?;
    m.add_function(wrap_pyfunction!(rainflow_py, m)?)?;
    Ok(())
}
