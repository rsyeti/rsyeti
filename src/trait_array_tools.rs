use std::f64;
use std::ops::Sub;
use ndarray::*;
use ndarray::{ArrayView1, Zip};
use num_traits::Num;
// use ndarray::parallel::prelude::*;

/// Parallel (not in place) elementwise diff operation for 1D array
/// 
/// Benchmarks 2x slower than numpy.diff() (which is pretty good)
pub fn diff<T>(array: &ArrayView1<T>) -> Array1<T> 
    where
    T: Num + Sync + Sub<Output = T>,
    {
    // TODO: Looking at doing this stuff with generics
    todo!();
    let result: Array1<T> = Zip::from(&array.slice(s![1..]))
        .and(&array.slice(s![..-1]))
        .par_map_collect(|x, y| x - y);

    result
}
