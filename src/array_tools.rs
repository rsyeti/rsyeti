use std::f64;
use ndarray::*;
use ndarray::parallel::prelude::*;
use ndarray::{ArrayView1, Zip, IxDyn};
use rayon::prelude::*;

use crate::find_unique_py;
// use ndarray::parallel::prelude::*;

/// Elementwise diff of array
/// 
/// # Arguments 
/// * `array` input array for elementwise differentiation
pub fn diff(array: &ArrayView1<f64>) -> Array1<f64> {
    &array.slice(s![1..]) - &array.slice(s![..-1])
}

/// In place parallel absolute value operation for array
fn abs(array: &mut Array1<f64>) -> (){
    // note here i need to dereference x using *x to assign value
    let _= array.par_map_inplace(|x| *x = x.abs());
}

/// Helper method to find max of ArrayView1<f64>
/// 
/// Ignore possible NaN values
pub fn absolute_value_maximum(array : &ArrayView1<f64>) -> f64 {
    let max = array 
        .iter()
        .fold(0_f64, |accum, x| x.abs().max(accum));
    max
}

/// Finds unique values in array based on tolerance set while not returning the diff  
/// 
/// Based on the method in [pyyeti](https://github.com/twmacro/pyyeti/blob/master/pyyeti/locate.py#L375)
///
/// # Arguments
/// * `array` times series array of values
/// * `tol` relative tolerance (default = 1e-6)
pub fn find_unique_neighbors(array: ArrayView1<f64>, tol: Option<f64>)-> Array1<bool> {
    if array.len() == 1 {
        return Array1::from_elem(array.len(), true)
    }
    let tol = match tol {
        Some(x) => x,
        None => 1e-6
    };
    let diff_arr = diff(&array.view());
    // NOTE: implemented max for float ignoring nan values here
    let max = absolute_value_maximum(&diff_arr.slice(s![..]));
    let stol = (tol * max).abs();
    let mut unique = Array1::from_elem((array.len(),), false);
    // setting first value to always be unique
    unique[0] = true;

    Zip::from(&diff_arr)
    .and(&diff_arr)
    .par_map_assign_into(&mut unique.slice_mut(s![1..]), |x,_| {
        x.abs() > stol 
    });
    unique
}

pub fn is_unique(diff: &f64, stol: &f64) -> bool{
    diff.abs() > *stol
}

pub fn is_peak(diff_curr: &f64, diff_prev: &f64) -> bool{
    diff_curr.is_sign_positive() != diff_prev.is_sign_positive()
}

/// Finds unique values in array based on tolerance set while also returning the diff  
/// 
/// Based on the method in [pyyeti](https://github.com/twmacro/pyyeti/blob/master/pyyeti/locate.py#L375)
///
/// # Arguments
/// * `array` times series array of values
/// * `tol` relative tolerance (default = 1e-6)
pub fn find_unique_neighbors_with_diff(array: &ArrayView1<f64>, tol: Option<f64>)-> (Array1<bool>, Array1<f64>) {
    let tol = match tol {
        Some(x) => x,
        None => 1e-6
    };
    let diff_arr = diff(&array.view());
    // using abs_max to avoid mutating the diff array
    let max = absolute_value_maximum(&diff_arr.slice(s![..]));
    let stol = (tol * max).abs();
    let mut unique = Array1::from_elem((array.len(),), false);
    // setting first value to always be unique
    unique[0] = true;

    Zip::from(&diff_arr)
    .and(&diff_arr)
    .par_map_assign_into(&mut unique.slice_mut(s![1..]), |x,_| {
        x.abs() > *&stol 
    });
    (unique, diff_arr)
}

/// Find unique indexes in array based on tolerance set while also returning the diff
/// 
/// # Arguments
/// * `array` times series array of values
/// * `tol` relative tolerance (default = 1e-6)
pub fn find_unique_neighbors_reduced(array: &ArrayView1<f64>, tol: Option<f64>)-> (Vec<usize>, Array1<f64>) {
    let tol = match tol {
        Some(x) => x,
        None => 1e-6
    };
    let diff_arr = diff(&array.view());
    // using abs_max to avoid mutating the diff array
    let max = absolute_value_maximum(&diff_arr.slice(s![..]));
    let stol = (tol * max).abs();
    let mut unique_indexes: Vec<usize> = Vec::with_capacity(diff_arr.len());
    unique_indexes.insert(0, 0);
    unique_indexes.par_extend((0usize..(diff_arr.len())).into_par_iter()
    .filter(|idx| &diff_arr[*idx].abs() > &stol)
    .map(|idx| idx + 1));
    (unique_indexes, diff_arr)
}


mod test {
    use super::*;
    #[test]
        fn test_find_unique_neighbors_reduced() {
            let arr = Array1::from_vec(vec![1.0, 2.0, 4.0, 4.0, 5.0]);
            let (indices, _)= find_unique_neighbors_reduced(&arr.view(), None);
            assert_eq!(Vec::from([0usize,1,2,4]), indices)

        }
//     fn test_diff() {
//         let arr = Array1::from_vec(vec![1.0, 2.0, 4.0, 5.0]);
//         let diff = diff(&arr);
//         let arr_correct = Array1::from_vec(vec![1.0, 2.0, 1.0]);
//         assert_eq!(diff, arr_correct);
//     }
//     #[test]
//     fn test_abs() {
//         let mut arr = Array1::from_vec(vec![1.0, -2.0, 5.0]);
//         let _ = abs(&mut arr);
//         let arr_correct = Array1::from_vec(vec![1.0, 2.0, 5.0]);
//         assert_eq!(arr, arr_correct);
//     }
//     #[test]
//     fn test_find_unique() {
//         let arr = Array1::from(vec![1.0, 2.0, 2.0, 3.0]);
//         let unique_arr = find_unique(arr, None);
//         assert_eq!(unique_arr, Array1::from(vec![true, true, false, true]))
//     }
}
