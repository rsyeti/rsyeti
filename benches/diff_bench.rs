use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use ndarray::arr1;
use ndarray::{s, Array1, ArrayView1, Zip};
use rand::{distributions::Uniform, Rng};

fn vectorized_diff(arr: &ArrayView1<f64>) -> Array1<f64> {
    &arr.slice(s![1..]) - &arr.slice(s![..-1])
}

fn parallel_diff(array: &ArrayView1<f64>) -> Array1<f64> {
    // The parallel method seemed to benchmark slightly faster than the non parallel method below although much more complicated
    Zip::from(&array.slice(s![1..]))
        .and(&array.slice(s![..-1]))
        .par_map_collect(|x, y| x - y)
    // Non parallelized method
    // &array.slice(s![1..]) - &array.slice(s![..-1])
}

fn diff_compare_bench(c: &mut Criterion) {
    // Example function for how to compare two function against each other
    let input_size = [1000, 100_000, 100_000_000];
    let mut group = c.benchmark_group("Diff Benches");
    for i in input_size.iter() {
        let range = Uniform::from(0..*i);
        let values: Vec<f64> = rand::thread_rng()
            .sample_iter(&range)
            .take(100)
            .map(f64::from)
            .collect();
        let arr = arr1(&values);
        group.bench_function(BenchmarkId::new("vecotorized_diff", i), |b| {
            b.iter(|| vectorized_diff(&arr.view()))
        });
        group.bench_function(BenchmarkId::new("parallel_diff", i), |b| b.iter(|| parallel_diff(&arr.view())));
    }
    group.finish();
}

criterion_group!(diff_bench, diff_compare_bench);
criterion_main!(diff_bench);
