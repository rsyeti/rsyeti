use criterion::{criterion_group, criterion_main, Criterion};
use ndarray::Array1;
use ndarray::ArrayView1;
use ndarray::arr1;
use rand::{distributions::Uniform, Rng};

use chuchuna::cycle_count::fast_find_alternating_peaks;
use chuchuna::cycle_count::find_alternating_peaks;
use chuchuna::cycle_count::reduce_arr_to_peaks;
use chuchuna::cycle_count::find_alternating_peaks_reduced;
use chuchuna::array_tools::{find_unique_neighbors_reduced, find_unique_neighbors_with_diff};

pub fn fast_find_bench(c: &mut Criterion) {
    let range = Uniform::from(0..100_000_000);
    let values: Vec<f64> = rand::thread_rng().sample_iter(&range).take(100).map(f64::from).collect();
    let arr = arr1(&values);
    let tol = Some(1e-6);
    c.bench_function("fast_find_bench", |b| b.iter(|| fast_find_alternating_peaks(&arr.view(), tol)));
}


pub fn wrapper(arr: &ArrayView1<f64>, tol: &Option<f64>) -> Array1<f64>{
    let (alternating_peaks_bools, peak_count) = find_alternating_peaks(&arr, &tol);
    let alternating_peaks = reduce_arr_to_peaks(arr, &alternating_peaks_bools, &peak_count);
    alternating_peaks
}

pub fn find_bench(c: &mut Criterion) {
    // Example function for how to benchmark a single function
    let range = Uniform::from(0..1_000_000);
    let values: Vec<f64> = rand::thread_rng().sample_iter(&range).take(100).map(f64::from).collect();
    let arr = arr1(&values);
    let tol = Some(1e-6);
    c.bench_function("find_bench", |b| b.iter(|| wrapper(&arr.view(), &tol)));
}

fn bench_findaps(c: &mut Criterion) {
    // Example function for how to compare two function against each other
    let range = Uniform::from(0..1_000_000);
    let values: Vec<f64> = rand::thread_rng().sample_iter(&range).take(100).map(f64::from).collect();
    let arr = arr1(&values);
    let tol = Some(1e-6);
    let mut group = c.benchmark_group("findap benches");
    group.bench_function("find_ap_reduced", |b| b.iter(|| find_alternating_peaks_reduced(&arr.view(), &tol)));
    group.bench_function("find_ap_normal", |b| b.iter(|| wrapper(&arr.view(), &tol)));
    group.bench_function("find_ap_fast", |b| b.iter(|| fast_find_alternating_peaks(&arr.view(),None)));
    group.bench_function("find_ap_fast (tol)", |b| b.iter(|| fast_find_alternating_peaks(&arr.view(),tol.clone())));
    group.finish();
}

fn bench_find_unique(c: &mut Criterion) {
    let range = Uniform::from(0..100_000_000);
    let values: Vec<f64> = rand::thread_rng().sample_iter(&range).take(100).map(f64::from).collect();
    let arr = arr1(&values);
    let tol = Some(1e-6);
    let mut group = c.benchmark_group("find_unique");
    group.bench_function("Reduced(no_tol)", |b| b.iter(|| find_unique_neighbors_reduced(&arr.view(),None)));
    group.bench_function("Reduced(tol)", |b| b.iter(|| find_unique_neighbors_reduced(&arr.view(),tol.clone())));
    group.bench_function("Normal(no_tol)", |b| b.iter(|| find_unique_neighbors_with_diff(&arr.view(),None)));
    group.bench_function("Normal(tol)", |b| b.iter(|| find_unique_neighbors_with_diff(&arr.view(),tol.clone())));


}

criterion_group!(peak_bench, bench_find_unique, bench_findaps);
criterion_main!(peak_bench);