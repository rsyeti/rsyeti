import time

import numpy as np
from pyyeti.locate import find_unique
from rsyeti import diff, find_unique_neighbors


def _test_unique():
    x = np.array([-1,2,3,3,4,5], dtype= np.float64)
    unique = find_unique_neighbors(x, tol = 1e-6)
    print(x)
    np.testing.assert_equal(unique, np.array([True, True, True, False, True, True]))

    x = np.array([-1100,2000,3000,-3000,3000,-1000,-1000], dtype= np.float64)
    unique = find_unique_neighbors(x, tol = 1e-6)
    print(x)
    np.testing.assert_equal(unique, np.array([True, True, True, True, True, True, False]))

def _test_diff():
    x = np.random.rand(1000) 
    correct_answer = np.diff(x)
    package_answer = diff(x)
    np.testing.assert_equal(correct_answer, package_answer)

def bench_diff():
    x = np.random.rand(10_000_000) * 10000.0

    start_time = time.time()
    _ = diff(x)
    total_time = time.time() - start_time
    print(f"rsyeti time {total_time}" )

    start_time = time.time()
    _ = np.diff(x)
    total_time = time.time() - start_time
    print(f"Numpy time {total_time:.5f}" )

    x = np.random.rand(10000)

    start_time = time.time()
    _ = diff(x)
    total_time = time.time() - start_time
    print(f"rsyeti time {total_time}" )

    start_time = time.time()
    _ = np.diff(x)
    total_time = time.time() - start_time
    print(f"Numpy time {total_time}" )


def bench_find_unique():
    x = np.random.rand(10_000_000) * 10000.0

    start_time = time.time()
    _ = find_unique_neighbors(x)
    total_time = time.time() - start_time
    print(f"rsyeti time {total_time}" )

    start_time = time.time()
    _ = find_unique(x)
    total_time = time.time() - start_time
    print(f"pyyeti time {total_time}" )

    x = np.random.rand(10000)

    start_time = time.time()
    _ = find_unique_neighbors(x)
    total_time = time.time() - start_time
    print(f"rsyeti time {total_time}" )

    start_time = time.time()
    _ = find_unique(x)
    total_time = time.time() - start_time
    print(f"pyyeti time {total_time}" )


if __name__ == "__main__":
    _test_diff()
    bench_diff()
    bench_find_unique()
    _test_unique()
