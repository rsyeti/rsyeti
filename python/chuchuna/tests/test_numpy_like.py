import pytest
import numpy as np

from chuchuna import diff

@pytest.mark.parametrize(
        "input_arr,expected",
        [
            (np.array([1,2,3,10,-11,12,13], dtype = np.float64),  np.diff(np.array([1,2,3,10,-11,12,13])))
        ]
)
def test_diff(input_arr, expected):
    package_answer = diff(input_arr)
    np.testing.assert_equal(expected, package_answer)



@pytest.mark.benchmark(group = 'diff')
def test_bench_np_diff_large_arr(benchmark):
    input_arr = np.random.rand(10_000_000) * 1000.0
    benchmark(np.diff, input_arr)

@pytest.mark.benchmark(group = 'diff')
def test_bench_diff_large_arr(benchmark):
    input_arr = np.random.rand(10_000_000) * 1000.0
    benchmark(diff, input_arr)

@pytest.mark.benchmark(group = 'small-diff')
def test_bench_np_diff_small_arr(benchmark):
    input_arr = np.random.rand(10_000) * 1000.0
    benchmark(np.diff, input_arr)

@pytest.mark.benchmark(group = 'small-diff')
def test_bench_diff_small_arr(benchmark):
    input_arr = np.random.rand(10_000) * 1000.0
    benchmark(diff, input_arr)