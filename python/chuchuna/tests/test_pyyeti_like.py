import pytest
from pyyeti.locate import find_unique
from pyyeti.cyclecount import findap
import numpy as np

from chuchuna import find_unique_neighbors, find_alternating_peaks, fast_find_alternating_peaks


BENCH_ARR = np.loadtxt("python/chuchuna/tests/test_arr.csv", delimiter=',')

@pytest.mark.parametrize(
        "input_arr,tol, expected",
        [
            (np.array([1,2,3,10,10,-11,12,13], dtype = np.float64), None, find_unique(np.array([1,2,3,10,10,-11,12,13]))),
            (np.array([1,2,3,10,10,-11,12,13], dtype = np.float64), 0.1, find_unique(np.array([1,2,3,10,10,-11,12,13]), 0.1))
        ]
)
def test_find_unique_neighbors(input_arr, tol, expected):
    if tol is None:
        output = find_unique_neighbors(input_arr )
    else:
        output = find_unique_neighbors(input_arr, tol)
    np.testing.assert_equal(expected, output)

@pytest.mark.parametrize(
        "input_arr,tol, expected",
        [
            (np.array([1,2,3,10,10,-11,12,13], dtype = np.float64), None, findap(np.array([1,2,3,10,10,-11,12,13]))),
            (np.array([1,2,3,10,10,-11,12,-13], dtype = np.float64), None, findap(np.array([1,2,3,10,10,-11,12,-13]))),
            (np.array([1,2,3,10,10,-11,12,-13], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,-13]), 0.1)),
            (np.array([1,2,3,10,10,-11,12,13], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,13]), 0.1)),
            (np.array([1,2,3,10,10,-11,12,13,14], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,13,14]), 0.1)),
            (np.array([1,2,3,10,10,-11,12,-13], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,-13]), 0.1)),
            (np.array([1, 2, 3, 4, 4, -2, -2, -2], dtype = np.float64), 0.1, findap(np.array([1, 2, 3, 4, 4, -2, -2, -2]), 0.1)),
            
        ]
)
def test_find_alternating_peaks(input_arr, tol, expected):
    if tol is None:
        output = find_alternating_peaks(input_arr )
    else:
        output = find_alternating_peaks(input_arr, tol)
    np.testing.assert_equal(expected, output)

@pytest.mark.parametrize(
        "input_arr,tol, expected",
        [
            (np.array([1,2,3,10,10,-11,12,13], dtype = np.float64), None, findap(np.array([1,2,3,10,10,-11,12,13]))),
            (np.array([1,2,3,10,10,-11,12,-13], dtype = np.float64), None, findap(np.array([1,2,3,10,10,-11,12,-13]))),
            (np.array([1,2,3,10,10,-11,12,-13], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,-13]), 0.1)),
            (np.array([1,2,3,10,10,-11,12,13], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,13]), 0.1)),
            (np.array([1,2,3,10,10,-11,12,13,14], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,13,14]), 0.1)),
            (np.array([1,2,3,10,10,-11,12,-13], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,-13]), 0.1)),
            (np.array([1,2,3,10,10,-11,12,12,-12,-12,-13], dtype = np.float64), 0.1, findap(np.array([1,2,3,10,10,-11,12,12,-12,-12,-13]), 0.1)),
            (np.array([1, 2, 3, 4, 4, -2, -2, -2], dtype = np.float64), 0.1, findap(np.array([1, 2, 3, 4, 4, -2, -2, -2]), 0.1)),
            
        ]
)
def test_fast_find_alternating_peaks(input_arr, tol, expected):
    if tol is None:
        output = fast_find_alternating_peaks(input_arr )
    else:
        output = fast_find_alternating_peaks(input_arr, tol)
    np.testing.assert_equal(expected, output)

@pytest.mark.benchmark(group = 'find_unique')
def test_bench_find_unique_neighbors(benchmark):
    benchmark(find_unique_neighbors, BENCH_ARR)

@pytest.mark.benchmark(group = 'find_unique')
def test_bench_pyyeti_find_unique(benchmark):
    benchmark(find_unique, BENCH_ARR)

@pytest.mark.benchmark(group = 'find_alternating_peaks_bool')
def test_bench_pyyeti_find_ap(benchmark):
    benchmark(findap, BENCH_ARR)
@pytest.mark.benchmark(group = 'find_alternating_peaks_bool')
def test_bench_find_ap(benchmark):
    benchmark(find_alternating_peaks, BENCH_ARR)
@pytest.mark.benchmark(group = 'find_alternating_peaks_bool')
def test_bench_fast_find_ap(benchmark):
    benchmark(fast_find_alternating_peaks, BENCH_ARR)

if __name__ == "__main__":
    input_arr = np.array([1,2,3,10,10,-11,12,13], dtype=np.float64)
    print(find_alternating_peaks(input_arr))
    
