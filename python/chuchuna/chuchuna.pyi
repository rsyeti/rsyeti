import numpy as np
def find_unique_neighbors(arr: np.ndarray, tol: float)-> np.ndarray : 
    """ Finds unique neighbors of array based on relative tolerance provided

    Returns:
        Boolean array of length arr, with true denoting the value is unique  
        and false denoting that the value is not unique
    """

def diff(arr: np.ndarray)-> np.ndarray : 
    """ Parallel element wise difference

    Note: It is faster to use numpy.diff() as this is an internal rust function

    Returns:
        #TODO
    """