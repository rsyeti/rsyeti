# chuchuna

# Objectives

The goal of this toolbox is not to replace the phenomenal [pyyeti](https://github.com/twmacro/pyyeti). The intent is to speed up some functionality available in pyyeti using Rust with pyo3 bindings.  This removes some of the issues with numba installation as well  


![Yeti by Philippe Semeria (www.philippe-semeria.com)](assets/yeti.jpg)

## Personal Goal

Writing python extensions using pyo3 looks like a very likely future for scientific computing.  I am using this library to learn the interface and improve my structural dynamics / software knowledge. 


# Development Setup
Need the following installed on your machine to get this to work:
  1. Poetry installed on your machine
  2. Rustup installed on your machine with cargo and all them tools

# Task Tracking
## New Development
- [ ] Implement rainflow cycle counting 1 for 1 from pyyeti
- [ ] Implement parallel SRS?

## Fix
- [ ] Need to replace parallel with vectorized diff based on benchmarks
- [ ] Check performance of parallel vs vectorized diff in python (maybe a C compile issue)

## Testing
- [ ] Rust side testing for all externally facing functions
- [ ] Python side testing versus pyyeti for all key functions
  - Note caveat being here the pyyeti bug

## Clean Up
- [ ] Scrub through and clean up public vs private functions
- [ ] Organize which functions belong in array tools and which do not


# Pyyeti Bug
- On an array with the following end points [.........10, -11, 12, 13]
- The issue is with how pyyeti deals with the last index in the loop for setting as true or false compared to the code here